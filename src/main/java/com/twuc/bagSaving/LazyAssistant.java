package com.twuc.bagSaving;

import java.util.*;
import java.util.stream.Collectors;

public class LazyAssistant extends Assistant {
    private List<Cabinet> cabinets = new ArrayList<>();

    public LazyAssistant(Cabinet... cabinet) {
        this.cabinets = Arrays.stream(cabinet).collect(Collectors.toList());
    }

    @Override
    public Ticket save(Bag bag) {
        LockerSize lockerSize = getBagLockerSize(bag);
        Ticket ticket = null;
        for (Cabinet cabinet : cabinets) {
            Map<LockerSize, Locker> lockers = cabinet.getLockers();
            if (lockers.get(lockerSize).getCapacity() == 0) {
                lockerSize = getLargerSize(bag, lockers);
            }
            if (Objects.isNull(lockerSize)) {
                lockerSize = getBagLockerSize(bag);
                continue;
            }
            ticket = cabinet.save(bag, lockerSize);
            if (!Objects.isNull(ticket)) {
                break;
            }
        }
        return ticket;
    }

    private LockerSize getLargerSize(Bag bag, Map<LockerSize, Locker> lockers) {
        LockerSize[] lockerSizes = LockerSize.values();
        LockerSize largerLockerSize = null;
        for (LockerSize lockerSize : lockerSizes) {
            if (lockerSize.getSizeNumber() > bag.getBagSize().getSizeNumber()
                    && lockers.get(lockerSize).getCapacity() != 0) {
                largerLockerSize = lockerSize;
                break;
            }
        }
        return largerLockerSize;
    }

    @Override
    public Bag getBag(Ticket ticket) {
        Bag bag = null;
        for (Cabinet cabinet : cabinets) {
            try {
                bag = cabinet.getBag(ticket);
            } catch (Exception e) {}
        }
        return bag;
    }
}
