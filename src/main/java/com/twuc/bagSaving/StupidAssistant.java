package com.twuc.bagSaving;

import java.util.*;
import java.util.stream.Collectors;

public class StupidAssistant extends Assistant {
    private List<Cabinet> cabinets = new ArrayList<>();

    public StupidAssistant(Cabinet cabinet) {
        this.cabinets.add(cabinet);
    }

    public StupidAssistant(Cabinet... cabinets) {
        this.cabinets = Arrays.stream(cabinets).collect(Collectors.toList());
    }

    @Override
    public Ticket save(Bag bag) {
        LockerSize lockerSize = getBagLockerSize(bag);
        Ticket ticket = null;

        for (int i = 0, cabinetsSize = cabinets.size(); i < cabinetsSize; i++) {
            Cabinet cabinet = cabinets.get(i);
            if (cabinet.getLockers().get(lockerSize).getCapacity() != 0) {
                ticket = cabinet.save(bag, lockerSize);
                break;
            } else if (i == cabinetsSize - 1) {
                throw new  InsufficientLockersException("");
            }
        }

        return ticket;
    }

    @Override
    public Bag getBag(Ticket ticket) {
        Bag bag = null;
        for (Cabinet cabinet : cabinets) {
            try {
                bag = cabinet.getBag(ticket);
            } catch (Exception e) {}
        }
        return bag;
    }
}
