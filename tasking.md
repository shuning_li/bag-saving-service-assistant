1、新建StupidAssistant用于帮助存储Bag  
2、处理没有合适Locker的错误  
3、新增StupidAssistant新的构造器，使其能接收多个Cabinet,存储Bag时按顺序查找合适的位置  
4、新增LazyAssistant类用于管理一个cabinet  
5、修改LazyAssistant构造器，接受多个cabinet，并根据包的大小存入对应Locker  
6、当前Cabinet不满足时存入下一个Cabinet中  
7、Bag可存入大点的Locker  